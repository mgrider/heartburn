//
//  HB_ViewController.m
//  HeartBurn
//
//  Created by Martin Grider on 1/25/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import "HB_ViewController.h"
#import "SimpleAudioEngine.h"


@interface HB_ViewController ()

@end


@implementation HB_ViewController

@synthesize uIState = _uIState;

@synthesize menuView = _menuView;
@synthesize playButton = _playButton;

@synthesize triangleUp = _triangleUp;
@synthesize triangleDown = _triangleDown;
@synthesize triangleLeft = _triangleLeft;
@synthesize triangleRight = _triangleRight;

@synthesize isUpPlayer = _isUpPlayer;
@synthesize isDownPlayer = _isDownPlayer;
@synthesize isLeftPlayer = _isLeftPlayer;
@synthesize isRightPlayer = _isRightPlayer;

@synthesize playerSelectionView = _playerSelectionView;
@synthesize playerSelectionUpsideDownLabel = _playerSelectionUpsideDownLabel;
@synthesize playerSelectionStartGameButton = _playerSelectionStartGameButton;

@synthesize gamePlayView = _gamePlayView;
@synthesize gamePlayColorLabel = _gamePlayColorLabel;
@synthesize gamePlaySymbolLabel = _gamePlaySymbolLabel;
@synthesize gamePausedView = _gamePausedView;
@synthesize gamePausedView2 = _gamePausedView2;
@synthesize gameTimer = _gameTimer;

@synthesize isStandardPlayingCards = _isStandardPlayingCards;
@synthesize gameNumberOfColors = _gameNumberOfColors;
@synthesize gameNumberOfSymbols = _gameNumberOfSymbols;
@synthesize gameTimerFireRate = _gameTimerFireRate;
@synthesize currentGameColor = _currentGameColor;
@synthesize currentGameSymbol = _currentGameSymbol;


#pragma mark - IBActions

- (IBAction)playerSelectionUpButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_PlayerSelection) {
		return;
	}
	if (_isUpPlayer) {
		if ([self numberOfPlayers] <= 2) {
			return;
		}
		[self hideUp];
		_isUpPlayer = NO;
	}
	else {
		[self showUp];
		_isUpPlayer = YES;
	}
	[self setPlayerSelectionButtonTextFromNumberOfPlayers];
}

- (IBAction)playerSelectionDownButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_PlayerSelection) {
		return;
	}
	if (_isDownPlayer) {
		if ([self numberOfPlayers] <= 2) {
			return;
		}
		[self hideDown];
		_isDownPlayer = NO;
	}
	else {
		[self showDown];
		_isDownPlayer = YES;
	}
	[self setPlayerSelectionButtonTextFromNumberOfPlayers];
}

- (IBAction)playerSelectionLeftButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_PlayerSelection) {
		return;
	}
	if (_isLeftPlayer) {
		if ([self numberOfPlayers] <= 2) {
			return;
		}
		[self hideLeft];
		_isLeftPlayer = NO;
	}
	else {
		[self showLeft];
		_isLeftPlayer = YES;
	}
	[self setPlayerSelectionButtonTextFromNumberOfPlayers];
}

- (IBAction)playerSelectionRightButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_PlayerSelection) {
		return;
	}
	if (_isRightPlayer) {
		if ([self numberOfPlayers] <= 2) {
			return;
		}
		[self hideRight];
		_isRightPlayer = NO;
	}
	else {
		[self showRight];
		_isRightPlayer = YES;
	}
	[self setPlayerSelectionButtonTextFromNumberOfPlayers];
}

- (void)setPlayerSelectionButtonTextFromNumberOfPlayers
{
	int nOP = [self numberOfPlayers];
	NSString *buttonTitle;
	switch (nOP) {
		case 2:
			buttonTitle = @"Start 2 Player Game";
			break;
		case 3:
			buttonTitle = @"Start 3 Player Game";
			break;
		case 4:
			buttonTitle = @"Start 4 Player Game";
			break;

		default:
			break;
	}
	[_playerSelectionStartGameButton setTitle:buttonTitle forState:UIControlStateNormal];
}

- (IBAction)playButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_Menu) {
		return;
	}
	_uIState = HB_GameState_PlayerSelection;
	[self hideMenuView];
	[self showPlayerSelectionScreen];
}

- (IBAction)startGameButtonPressed:(id)sender
{
	if (_uIState != HB_GameState_PlayerSelection) {
		return;
	}
	_uIState = HB_GameState_Playing;
	[self hidePlayerSelectionScreen];
	[self startTheGame];
}

- (IBAction)pauseButtonPressed:(id)sender
{
	NSLog(@"getting pauseButton event.");
	if (_uIState == HB_GameState_Playing) {
		_uIState = HB_GameState_Paused;
		[self showGamePausedView];
		[_gameTimer invalidate];
	}
	else if (_uIState == HB_GameState_Paused) {
		_uIState = HB_GameState_Playing;
		[self hideGamePausedView];
		_gameTimer = [NSTimer scheduledTimerWithTimeInterval:_gameTimerFireRate
													  target:self
													selector:@selector(continueTheGame)
													userInfo:nil
													 repeats:NO];
	}
}

- (IBAction)pauseQuitButtonPressed:(id)sender
{
	if (_uIState == HB_GameState_Paused) {
		_uIState = HB_GameState_Menu;
		[self showMenuView];
		[self hideAllPlayerDirections];
		[self hideGamePausedView];
		[self hideGamePlayView];
	}
}


#pragma mark - playing the game

- (void)startTheGame
{
	if (_uIState != HB_GameState_Playing) {
		return;
	}
	_gameTimerFireRate = HB_GameTimer_MaximumFireDelay;

	[self assignRandomCurrentPlayerDirection];
	[self hideAllPlayerDirections];
	[self showDirection:_currentPlayerDirection];
	[self showRandomGameCard];
	[self showGamePlayView];

	_gameTimer = [NSTimer scheduledTimerWithTimeInterval:_gameTimerFireRate
												  target:self
												selector:@selector(continueTheGame)
												userInfo:nil
												 repeats:NO];
}

- (void)continueTheGame
{
	if (_uIState != HB_GameState_Playing) {
		return;
	}

	float pitch = [self pitchFromTimerInterval];
	[[SimpleAudioEngine sharedEngine] playEffect:@"heartbeat-short.wav"
										   pitch:pitch
											 pan:0.0f
											gain:1.0f];

	[self incrementGameTimerFireRate];
	[self showNextPlayerIndicator];
	[self showRandomGameCard];

	NSLog(@"pitch is %f for fire rate %f", pitch, _gameTimerFireRate);

	_gameTimer = [NSTimer scheduledTimerWithTimeInterval:_gameTimerFireRate
												  target:self
												selector:@selector(continueTheGame)
												userInfo:nil
												 repeats:NO];
}

- (float)pitchFromTimerInterval
{
	static float timerSinglePercent = (HB_GameTimer_MaximumFireDelay - HB_GameTimer_MinimumFireDelay) / 100.0f;
	float timerPercent = 100.0f - ((_gameTimerFireRate - HB_GameTimer_MinimumFireDelay) / timerSinglePercent);
	static float pitchSinglePercent = (HB_GameAudio_MaximumHeartbeatPitch - HB_GameAudio_MinimumHeartbeatPitch) / 100.0f;
	float newPitch = (pitchSinglePercent * timerPercent) + HB_GameAudio_MinimumHeartbeatPitch;
	if (newPitch < HB_GameAudio_MaximumHeartbeatPitch) {
		return newPitch;
	}
	else {
		return HB_GameAudio_MaximumHeartbeatPitch;
	}
}

- (void)incrementGameTimerFireRate
{
	static float timerSinglePercent = (HB_GameTimer_MaximumFireDelay - HB_GameTimer_MinimumFireDelay) / 100.0f;
	static float turnPercent = 100.0f / HB_Game_NumberOfExpectedTurns;
	float newFireRate = _gameTimerFireRate - (timerSinglePercent * turnPercent);
	_gameTimerFireRate = (newFireRate > HB_GameTimer_MinimumFireDelay) ? newFireRate : HB_GameTimer_MinimumFireDelay;
}

- (void)assignRandomCurrentPlayerDirection
{
	int numberOfPlayers = [self numberOfPlayers];
	int dir = arc4random_uniform(numberOfPlayers);
	_currentPlayerDirection = (HB_Game_PlayerDirection)dir;
}

- (int)numberOfPlayers
{
	int numberOfPlayers = 0;
	if (_isUpPlayer) {
		numberOfPlayers += 1;
	}
	if (_isDownPlayer) {
		numberOfPlayers += 1;
	}
	if (_isLeftPlayer) {
		numberOfPlayers += 1;
	}
	if (_isRightPlayer) {
		numberOfPlayers += 1;
	}
	return numberOfPlayers;
}

- (void)showNextPlayerIndicator
{
	[self hideDirection:_currentPlayerDirection];
	_currentPlayerDirection = [self playerDirectionAfterCurrentPlayerDirection];
	[self showDirection:_currentPlayerDirection];
}

- (HB_Game_PlayerDirection)playerDirectionAfterCurrentPlayerDirection
{
	HB_Game_PlayerDirection newDir;
	switch (_currentPlayerDirection) {
		case HB_Game_PlayerDirection_Up: {
			if (_isRightPlayer) {
				newDir = HB_Game_PlayerDirection_Right;
			}
			else if (_isDownPlayer) {
				newDir = HB_Game_PlayerDirection_Down;
			}
			else if (_isLeftPlayer) {
				newDir = HB_Game_PlayerDirection_Left;
			}
			break;
		}
		case HB_Game_PlayerDirection_Down: {
			if (_isLeftPlayer) {
				newDir = HB_Game_PlayerDirection_Left;
			}
			else if (_isUpPlayer) {
				newDir = HB_Game_PlayerDirection_Up;
			}
			else if (_isRightPlayer) {
				newDir = HB_Game_PlayerDirection_Right;
			}
			break;
		}
		case HB_Game_PlayerDirection_Left: {
			if (_isUpPlayer) {
				newDir = HB_Game_PlayerDirection_Up;
			}
			else if (_isRightPlayer) {
				newDir = HB_Game_PlayerDirection_Right;
			}
			else if (_isDownPlayer) {
				newDir = HB_Game_PlayerDirection_Down;
			}
			break;
		}

		case HB_Game_PlayerDirection_Right:
		default: {
			if (_isDownPlayer) {
				newDir = HB_Game_PlayerDirection_Down;
			}
			else if (_isLeftPlayer) {
				newDir = HB_Game_PlayerDirection_Left;
			}
			else if (_isUpPlayer) {
				newDir = HB_Game_PlayerDirection_Up;
			}
			break;
		}
	}
	return newDir;
}


#pragma mark - showing game cards

- (void)showRandomGameCard
{
	// color
	int newColor = arc4random_uniform(_gameNumberOfColors);
	while(newColor == _currentGameColor) {
		newColor = arc4random_uniform(_gameNumberOfColors);
	}
	_currentGameColor = newColor;
	[self showCurrentColor];

	// symbol
	int newSymbol = arc4random_uniform(_gameNumberOfSymbols);
	while(newSymbol == _currentGameSymbol) {
		newSymbol = arc4random_uniform(_gameNumberOfSymbols);
	}
	_currentGameSymbol = newSymbol;
	[self showCurrentSymbol];
}

- (void)showCurrentColor
{
	// color == suite ♠♣♥♦
	if (_isStandardPlayingCards) {
		switch (_currentGameColor) {
			case 0:
				[_gamePlayColorLabel setText:@"♠"];
				[_gamePlayColorLabel setTextColor:[UIColor blackColor]];
				break;
			case 1:
				[_gamePlayColorLabel setText:@"♣"];
				[_gamePlayColorLabel setTextColor:[UIColor blackColor]];
				break;
			case 2:
				[_gamePlayColorLabel setText:@"♥"];
				[_gamePlayColorLabel setTextColor:[UIColor redColor]];
				break;
			case 3:
				[_gamePlayColorLabel setText:@"♦"];
				[_gamePlayColorLabel setTextColor:[UIColor redColor]];
				break;

			default:
				break;
		}
	}
}

- (void)showCurrentSymbol
{
	// symbol == card
	if (_isStandardPlayingCards) {
		switch (_currentGameSymbol) {
			case 0:
				[_gamePlaySymbolLabel setText:@"A"];
				break;
			case 1:
				[_gamePlaySymbolLabel setText:@"K"];
				break;
			case 2:
				[_gamePlaySymbolLabel setText:@"Q"];
				break;
			case 3:
				[_gamePlaySymbolLabel setText:@"J"];
				break;
			case 4:
				[_gamePlaySymbolLabel setText:@"10"];
				break;
			case 5:
				[_gamePlaySymbolLabel setText:@"9"];
				break;

			default:
				break;
		}
	}
}


#pragma mark - showing/hiding player directions

- (void)showAllPlayerDirections
{
	[self showLeft];
	[self showRight];
	[self showUp];
	[self showDown];
}

- (void)hideAllPlayerDirections
{
	[self hideLeft];
	[self hideRight];
	[self hideUp];
	[self hideDown];
}

- (void)hideDirection:(HB_Game_PlayerDirection)direction
{
	switch (direction) {
		case HB_Game_PlayerDirection_Up:
			[self hideUp];
			break;
		case HB_Game_PlayerDirection_Down:
			[self hideDown];
			break;
		case HB_Game_PlayerDirection_Left:
			[self hideLeft];
			break;
		case HB_Game_PlayerDirection_Right:
			[self hideRight];
			break;
			
		default:
			break;
	}
}

- (void)showDirection:(HB_Game_PlayerDirection)direction
{
	switch (direction) {
		case HB_Game_PlayerDirection_Up:
			[self showUp];
			break;
		case HB_Game_PlayerDirection_Down:
			[self showDown];
			break;
		case HB_Game_PlayerDirection_Left:
			[self showLeft];
			break;
		case HB_Game_PlayerDirection_Right:
			[self showRight];
			break;

		default:
			break;
	}
}

- (void)showLeft
{
	[_triangleLeft setHidden:NO];
}

- (void)showRight
{
	[_triangleRight setHidden:NO];
}

- (void)showUp
{
	[_triangleUp setHidden:NO];
}

- (void)showDown
{
	[_triangleDown setHidden:NO];
}

- (void)hideLeft
{
	[_triangleLeft setHidden:YES];
}

- (void)hideRight
{
	[_triangleRight setHidden:YES];
}

- (void)hideUp
{
	[_triangleUp setHidden:YES];
}

- (void)hideDown
{
	[_triangleDown setHidden:YES];
}


#pragma mark - switching game "states"

- (void)hideMenuView
{
	[_menuView setHidden:YES];
}

- (void)showMenuView
{
	[_menuView setHidden:NO];
}

- (void)hidePlayerSelectionScreen
{
	[_playerSelectionView setHidden:YES];
}

- (void)showPlayerSelectionScreen
{
	[_playerSelectionView setHidden:NO];
	[self showAllPlayerDirections];
	_isRightPlayer = YES;
	_isLeftPlayer = YES;
	_isDownPlayer = YES;
	_isUpPlayer = YES;
	[self setPlayerSelectionButtonTextFromNumberOfPlayers];
}

- (void)showGamePlayView
{
	[_gamePlayView setHidden:NO];
}

- (void)hideGamePlayView
{
	[_gamePlayView setHidden:YES];
}

- (void)showGamePausedView
{
	[_gamePausedView setHidden:NO];
	[_gamePausedView2 setHidden:NO];
}

- (void)hideGamePausedView
{
	[_gamePausedView setHidden:YES];
	[_gamePausedView2 setHidden:YES];
}


#pragma mark - boilerplate

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	_playerSelectionUpsideDownLabel.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180));
	_isRightPlayer = YES;
	_isLeftPlayer = YES;
	_isDownPlayer = YES;
	_isUpPlayer = YES;

	_isStandardPlayingCards = YES;
	_gameNumberOfSymbols = 6;
	_gameNumberOfColors = 4;

	[[_playerSelectionStartGameButton titleLabel] setNumberOfLines:0];
	[[_playerSelectionStartGameButton titleLabel] setLineBreakMode:NSLineBreakByWordWrapping];
	[[_playerSelectionStartGameButton titleLabel] setTextAlignment:NSTextAlignmentCenter];

	[self hidePlayerSelectionScreen];
	[self hideAllPlayerDirections];
	[self hideGamePlayView];
	[self hideGamePausedView];
	[self showMenuView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
	[self setTriangleUp:nil];
	[self setTriangleDown:nil];
	[self setTriangleLeft:nil];
	[self setTriangleRight:nil];
	[self setMenuView:nil];
	[self setPlayerSelectionView:nil];
	[self setPlayerSelectionUpsideDownLabel:nil];
	[self setPlayerSelectionStartGameButton:nil];
	[self setGamePlayView:nil];
	[self setGamePlaySymbolLabel:nil];
	[self setGamePlayColorLabel:nil];
	[self setGamePausedView:nil];
	[self setGamePausedView2:nil];
	[super viewDidUnload];
}
@end
