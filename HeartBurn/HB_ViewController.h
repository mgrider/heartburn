//
//  HB_ViewController.h
//  HeartBurn
//
//  Created by Martin Grider on 1/25/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
	HB_GameState_Menu,
	HB_GameState_PlayerSelection,
	HB_GameState_Playing,
	HB_GameState_Paused,
	HB_GameState_
} HB_GameState;

typedef enum {
	HB_Game_PlayerDirection_Up,
	HB_Game_PlayerDirection_Down,
	HB_Game_PlayerDirection_Left,
	HB_Game_PlayerDirection_Right
} HB_Game_PlayerDirection;

#define HB_GameTimer_MinimumFireDelay 0.9f
#define HB_GameTimer_MaximumFireDelay 1.5f

#define HB_GameAudio_MinimumHeartbeatPitch 0.7f
#define HB_GameAudio_MaximumHeartbeatPitch 1.5f

#define HB_Game_NumberOfExpectedTurns 25.0f

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)


@interface HB_ViewController : UIViewController


@property (assign) HB_GameState uIState;

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIImageView *triangleUp;
@property (weak, nonatomic) IBOutlet UIImageView *triangleDown;
@property (weak, nonatomic) IBOutlet UIImageView *triangleLeft;
@property (weak, nonatomic) IBOutlet UIImageView *triangleRight;
@property (assign) BOOL isUpPlayer;
@property (assign) BOOL isDownPlayer;
@property (assign) BOOL isLeftPlayer;
@property (assign) BOOL isRightPlayer;
@property (assign) HB_Game_PlayerDirection currentPlayerDirection;

@property (weak, nonatomic) IBOutlet UIView *playerSelectionView;
@property (weak, nonatomic) IBOutlet UILabel *playerSelectionUpsideDownLabel;
@property (weak, nonatomic) IBOutlet UIButton *playerSelectionStartGameButton;

@property (weak, nonatomic) IBOutlet UIView *gamePlayView;
@property (weak, nonatomic) IBOutlet UILabel *gamePlaySymbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *gamePlayColorLabel;
@property (weak, nonatomic) IBOutlet UIView *gamePausedView;
@property (weak, nonatomic) IBOutlet UIView *gamePausedView2;
@property (strong, nonatomic) NSTimer *gameTimer;

@property (assign) BOOL isStandardPlayingCards;
@property (assign) int gameNumberOfColors;
@property (assign) int gameNumberOfSymbols;
@property (assign) float gameTimerFireRate;
@property (assign) int currentGameColor;
@property (assign) int currentGameSymbol;


- (IBAction)playerSelectionUpButtonPressed:(id)sender;
- (IBAction)playerSelectionDownButtonPressed:(id)sender;
- (IBAction)playerSelectionLeftButtonPressed:(id)sender;
- (IBAction)playerSelectionRightButtonPressed:(id)sender;

- (IBAction)playButtonPressed:(id)sender;
- (IBAction)startGameButtonPressed:(id)sender;

- (IBAction)pauseButtonPressed:(id)sender;
- (IBAction)pauseQuitButtonPressed:(id)sender;


@end
