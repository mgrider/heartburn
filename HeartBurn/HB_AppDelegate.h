//
//  HB_AppDelegate.h
//  HeartBurn
//
//  Created by Martin Grider on 1/25/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HB_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
