//
//  main.m
//  HeartBurn
//
//  Created by Martin Grider on 1/25/13.
//  Copyright (c) 2013 Abstract Puzzle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HB_AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([HB_AppDelegate class]));
	}
}
